libdevel-backtrace-perl (0.12-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Rene Mayorga from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:46:27 +0100

libdevel-backtrace-perl (0.12-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 14:16:12 +0100

libdevel-backtrace-perl (0.12-2) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Roberto C. Sanchez ]
  * Remove /me from Uploaders.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Fix grammar error in package description. Thanks to Clayton Casciato
    for the bug report and the patch. (Closes: #687363)

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Switch to source format "3.0 (quilt)".
  * Add explicit build dependency on libmodule-build-perl.
  * debian/{rules,control,compat}: debhelper 9 and dh(1).
  * Update years of packaging copyright.
  * Update license stanzas in debian/copyright.
  * Fix hashbangs in example scripts.
  * Add a patch to fix a spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Tue, 09 Jun 2015 23:34:23 +0200

libdevel-backtrace-perl (0.12-1) unstable; urgency=low

  [ Tim Retout ]
  * debian/copyright: Fix 'at your opinion' typo.

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Rene Mayorga ]
  * New upstream release
  * debian/control
    + use debhelper version 7
    + remove libmodule-build-perl from B-D-I, dh7 prefers MakeMaker
    + update my email address
  * debian/rules; refresh it, using dh-make-perl -R
  * debian/copyright:
    + update the proposal format
    + mention individual debian/* copyright info based on debian/changelog

 -- Rene Mayorga <rmayorga@debian.org>  Wed, 28 Jan 2009 20:30:22 -0600

libdevel-backtrace-perl (0.11-1) unstable; urgency=low

  [ Rene Mayorga ]
  * New upstream release
  * debian/control:
    + Bump Standards-Version to 3.8.0 (No changes needed)
    + Add myself to uploaders
  * refresh debian/rules with dh-make-perl -R; remove some comments
    from template
  * Add Copyright from upstream to debian/copyright and add stanza to Debian
    perl group

  [ gregor herrmann ]
  * debian/control: change my email address.

 -- Rene Mayorga <rmayorga@debian.org.sv>  Mon, 07 Jul 2008 23:59:10 -0600

libdevel-backtrace-perl (0.10-2) unstable; urgency=low

  * Extend long description, thanks to Gerfried Fuchs for the bug report
    (closes: #475995).
  * debian/rules: remove some comments; don't install README any more (just
    a text version of the inline POD documentation).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 14 Apr 2008 16:55:58 +0200

libdevel-backtrace-perl (0.10-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 02 Apr 2008 21:46:23 +0200

libdevel-backtrace-perl (0.09-1) unstable; urgency=low

  [ Roberto C. Sanchez ]
  * Added watch file.
  * New upstream release.
  * Place Debian packaging in PD to match module license.

  [ gregor herrmann ]
  * debian/copyright: adapt according to upstream changes.
  * Add /me to Uploaders.
  * debian/rules: minor updates.

 -- Roberto C. Sanchez <roberto@connexer.com>  Sun, 30 Mar 2008 15:24:45 -0400

libdevel-backtrace-perl (0.05-1) unstable; urgency=low

  * Initial Release (Closes: #471574).

 -- Roberto C. Sanchez <roberto@connexer.com>  Tue, 18 Mar 2008 20:49:50 -0400
